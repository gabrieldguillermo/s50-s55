import Banner from '../components/Banner';


export default function Error() {
	const data= {
			title:"404",
			 msg:"Page Not Found",
			 link:"/" ,
			 button: "Go back to homepage"
	}
	return(
		<div align="center">
			<Banner data= {data}/>
		</div>
		
	)
}