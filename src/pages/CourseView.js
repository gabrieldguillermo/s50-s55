import {useState,useEffect, useContext } from 'react';
import {Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView(){

	const{ user } = useContext(UserContext)

	const navigate = useNavigate();

	const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll= (courseId) => {
		fetch('http://localhost:4000/users/enroll',{
			method:"POST",
			headers:{
				'Content-type':"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId:courseId
			})
		})
		.then(res => res.json())
		.then(data => {
				console.log(data)
				if(data === true ){
					Swal.fire({
					title:"Successfully Enrolled",
					icon:"success",
					text: "You have Successfully Enrolled!"
				})

				navigate("/courses")

			} else {
					Swal.fire({
				title:"Something went wrong",
				icon:"error",
				text: "Please try again!"
			})
			}
		})
	}

	useEffect(()=>{
		console.log(courseId);
		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	},[courseId])
	return(
		<Container className= "mt-5">
			<Row >
				<Col  md={12} lg={4} >
					<Card className= "mb-5 shadow">
				      <Card.Body>
				        <Card.Title className=" fw-bold">{name}</Card.Title>
				          	<Card.Subtitle className=" fw-bold mb-1">Description:</Card.Subtitle>
				        	<Card.Text className="mb-4">{description}</Card.Text>
							<Card.Text className="fs-6 fw-bold mb-1">Price:</Card.Text>
							<Card.Text>Php {price.toLocaleString()}</Card.Text>
							<Card.Subtitle> Class Schedule:</Card.Subtitle>
							<Card.Text>5:30 - 9:30</Card.Text>
				        	{
				        		(user.id !== null) ?
				        			<Button className="bg-primary" onClick={() => enroll(courseId)}>Enroll</Button>
				        			: <Button className="bg-primary"as={Link} to="/login" >Login to Enroll</Button>
				        	}
				        
				      </Card.Body>
				   	</Card>
				</Col>
			</Row>
		</Container>
	)
}