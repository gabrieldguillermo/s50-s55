import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Accordion from '../components/Resources';

	
export default function Home(){
	const data = {
			title:"Zuitt Coding Bootcamp", 
			msg:"Opportunities for everyone, everywhere.",
			link:"/courses" ,
			button:"Enroll Now!"
		}
		const test = 'this is a test';

	return (
		<>			
			{/*JSX*/}
			<Banner data={data} test={test}/>
			
			{/*Vanilla javascript*/}
			{/*{ Banner({data,test}) }*/}


			<Highlights/>
			<Accordion/>
		</>
	)
};