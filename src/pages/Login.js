import {useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

 
 	function  loginUser(e){
 		e.preventDefault();

 		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
 			method:"POST",
 			headers:{
 				'Content-type': 'application/json'
 			},
 			body: JSON.stringify({
 				email:email,
 				password: password
 			})
 		})
 		.then( res => res.json())
 		.then(data => {
 			console.log(data);

 			if(typeof data.access!== "undefined"){
 				localStorage.setItem('token', data.access);
 				retrieveUserDetails(data.access)

 				Swal.fire({
 					title:"Login Succesfull",
 					icon: "success",
 					text:"Welcome to Zuitt!"
 				})
 			} else {

 				Swal.fire({
 					title:"Authentication Failed",
 					icon: "error",
 					text:"Please, check your login details and try again!"
 				})


 			}
 		})

 
 			// localStorage.setItem("email",email);
 			// setUser({email: localStorage.getItem('email')})
 			setEmail("");
 			setPassword("");
 			// console.log(`${email} has been verified!`)
 			// alert('User Succesfully log in!');
 		
 		
 	}
 	const retrieveUserDetails = (token) => {

 		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
 			headers:{
 				Authorization: `Bearer ${ token }`
 			}
 		})
 		.then( res => res.json())
 		.then(data => {
 			console.log(data);
 			setUser({
 				id: data._id,
 				isAdmin: data.isAdmin
 			})

 		})
 	};
 


 	useEffect(() => {
 		if(email !== "" && password !== "" ) {
 			setIsActive(true)
 		}else{
 			setIsActive(false)
 		}
 	},[email,password]);

    return (

    	(user.id !== null) ?
    		<Navigate to="/courses" />
    	:

        <Form className="mt-5 border shadow p-3 rounded" onSubmit= {(e)=> loginUser(e)}>
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	value ={email}
	        	onChange={(e)=> setEmail(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="Enter Your Email"
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	value={password}
	        	onChange={(e)=> setPassword(e.target.value.replace(/\s/g, "")) }
	        	placeholder="Enter Your Password"
	        	required 
	        	autoComplete="off"
	        	 />
	        	
	      </Form.Group>
	     	{ isActive ?
	     		 <Button variant="success" type="submit" id="submitBtn">
	       		 Submit
	     		 </Button>
	     		 :
	     		 <Button variant="success" type="submit" id="submitBtn" disabled>
	       		 Submit
	     		 </Button>
	     	}
	    </Form>

    );
}