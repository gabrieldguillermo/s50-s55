import { Fragment,useState, useEffect } from 'react';
import {Row} from 'react-bootstrap';

//course object data
import CourseCard from '../components/CourseCard';
// import CourseData from '../data/coursesData';

	
export default function Courses() {
	// const courseCardElem = CourseData.map( course => {	
	// 	return <CourseCard key={course.id} price={course.price.toLocaleString()} props={course}/>
	// });
	
	const [course,setCourses] = useState([]);
	useEffect(()=>{
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setCourses(data.map(course => {
		return(
			<CourseCard key={course._id} courseProp={course}/>
			)}
			))
		})
	},[])

	return(
		<>
			<h3 align="center" className="mt-5" >OFFERED COURSES</h3>
			<Row>
	 			{course}
 			</Row>	
		</>
		)
};

