import {Row, Col, Card } from 'react-bootstrap';


export default function Highlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className ="cardHighlight p-3">
		     		<Card.Body>
		       		<Card.Title>Learn From Home</Card.Title>
		       <Card.Text>
		        Learn coding while in the comfort of your home
		       </Card.Text>
		    </Card.Body>
			</Card>		
	
			</Col>

			<Col xs={12} md={4}>
				<Card className ="cardHighlight p-3">
		     		<Card.Body>
		       		<Card.Title>Study Now, Pay Later</Card.Title>
		       <Card.Text>
		        Finish the bootcamp first, Pay later
		       </Card.Text>
		    </Card.Body>
			</Card>		
	
			</Col>

<Col xs={12} md={4}>
				<Card className ="cardHighlight p-3">
		     		<Card.Body>
		       		<Card.Title>Be part of our Community</Card.Title>
		       <Card.Text>
		        Apply as an Instructor and share your knowledge and experience with other aspiring developer
		       </Card.Text>
		    </Card.Body>
			</Card>		
	
			</Col>
		</Row>
	)
}