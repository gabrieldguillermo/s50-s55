import {Accordion} from 'react-bootstrap';

export default function accordion() {
  return (
    <Accordion  className="mb-5">
      <Accordion.Item eventKey="0">
        <Accordion.Header>HTML</Accordion.Header>
        <Accordion.Body>
         	 HTML stands for HyperText Markup Language. It is a standard markup language for web page creation. It allows the creation and structure of sections, paragraphs, and links using HTML elements (the building blocks of a web page) such as tags and attributes. 
          <div className="mt-3">
          		<a href="https://www.hostinger.ph/tutorials/what-is-html" target="_blank" rel="noreferrer">Read More</a>
          </div>
        </Accordion.Body>

      </Accordion.Item>
      <Accordion.Item eventKey="1">
        <Accordion.Header>CSS</Accordion.Header>
        <Accordion.Body>
          CSS stands for Cascading Style Sheets language and is used to stylize elements written in a markup language such as HTML. It separates the content from the visual representation of the site. The relation between HTML and CSS is strongly tied together since HTML is the very foundation of a site and CSS is all of the aesthetics of an entire website.
          <div className="mt-3">
          		<a href="https://www.hostinger.ph/tutorials/what-is-css" target="_blank" rel="noreferrer">Read More</a>
          </div>
        </Accordion.Body>
      </Accordion.Item>
          <Accordion.Item eventKey="3">
        <Accordion.Header>JAVASCRIPT</Accordion.Header>
        <Accordion.Body>
          JavaScript is a scripting language for creating dynamic web page content. It creates elements for improving site visitors’ interaction with web pages, such as dropdown menus, animated graphics, and dynamic background colors.
            <div className="mt-3">
          		<a href="https://www.hostinger.ph/tutorials/what-is-javascript" target="_blank" rel="noreferrer">Read More</a>
          </div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}


