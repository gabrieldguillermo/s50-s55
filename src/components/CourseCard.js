// import {useState,useEffect} from 'react';
import { Col, Card, Button} from 'react-bootstrap';
import {Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	const {name, description, price, _id ,enrollees} =courseProp;

	/*
		Use the state hook for this component to be able to store its state.
		States ars used to track of the information related to individual

		SYNTAX:
			const [getter, setter] = useState(initialGetterValue);

	*/
	//  state hook to store the state of enrollees
	// const [count, setCount] = useState(0);
	// const [seats, setSeats]= useState(10);
	// console.log(useState(0));

    // function enroll(){
    //   if (seats > 0 ) {
    //     setCount(count + 1)
    //     console.log('Enrollees: ' + count)
    //     setSeats(seats - 1)
    //     console.log('Seats: ' + seats)
    //   } //else {
    //     // alert("No more seats available.")
    //   // }
    // };

    // useEffect(()=> {
    // 	if (seats === 0 ) {
    // 		alert("No more seats available.")
    // 	}
    // },[seats])

	
	return ( 
		<Col md={12} lg={4} >
			<Card className= "mb-5 shadow">
		      <Card.Body>
		        <Card.Title className=" fw-bold">{name}</Card.Title>
		          	<Card.Subtitle className=" fw-bold mb-1">Description:</Card.Subtitle>
		        	<Card.Text className="mb-4">{description}</Card.Text>
					<Card.Text className="fs-6 fw-bold mb-1">Price:</Card.Text>
					<Card.Text>Php {price.toLocaleString()}</Card.Text>
					{/*<Card.Text>Available seats : { seats}</Card.Text>*/}
					<Card.Text>Enrolless: {enrollees.length}</Card.Text>
		        	<Button className= " bg-primary"  as={Link} to={`/courses/${_id}`} >Details</Button>
		        
		      </Card.Body>
		   	</Card>
		</Col>
	) 
};






