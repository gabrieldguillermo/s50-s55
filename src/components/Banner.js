import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner({data,test}) {
	const {title, msg, link, button} = data
	
    return (
        <Row>
			<Col className="p-5">		
					<h1>{title}</h1>
					<p>{msg}</p>
					<h3>{test}</h3>					
					<Button variant="primary" as={Link} to={link}>{button} </Button>
			</Col>
		</Row>
    )
}