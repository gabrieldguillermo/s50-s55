// export default [
// 	{
// 		name: "HTML",
// 		description: "HTML 101",
// 		price: 10000
// 	},
// 	{
// 		name: "CSS",
// 		description: "CSS 101",
// 		price:13000
// 	},
// 	{
// 		name: "JAVASCRIPT",
// 		description: "JAVASCRIPT 101",
// 		price:18000
// 	},
// 	{
// 		name: "PHP",
// 		description: "PHP 101",
// 		price:15000
// 	}

// ]

const coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 55000,
		onOffer: true
	}

]

export default coursesData;
